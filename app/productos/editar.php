<?php 
if(isset($_GET['id'])){
    require_once ("database/productos.php");
    $productosDB = new Productos;
    $result = $productosDB->getID($_GET['id']);
    if($result->num_rows > 0){
        $productoEdit = $result->fetch_assoc();

        if(isset($_POST['editarProducto']['nombre'], $_POST['editarProducto']['estado'])) {
            if($productosDB->edit($productoEdit['id'], $_POST['editarProducto'])){
                echo '<script type="text/javascript">window.location.href = "/productos";</script>';
            }else {
                $message = true;
            }
        }

    }else{
        echo '<script type="text/javascript">window.location.href = "/productos";</script>';
    }
}else {
    echo '<script type="text/javascript">window.location.href = "/productos";</script>';
}
?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Editar Producto</h3>
                </div>
                <form role="form" action="" method="post" name="formUsuario">
                    <div class="box-body row">
                        <div class="form-group col-md-6">
                            <label for="nombre">Nombre</label>
                            <input type="text" class="form-control" name="editarProducto[nombre]" placeholder="Nombre" value="<?php echo($productoEdit['nombre']); ?>" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Estado</label>
                            <select class="form-control" name="editarProducto[estado]">
                                <option value="1" <?php echo(($productoEdit['estado'])? 'selected' : ''); ?>>Activo</option>
                                <option value="0" <?php echo(($productoEdit['estado'])? '': 'selected'); ?>>Inactivo</option>
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="descripcion">Descripcion</label>
                            <textarea rows="3"  class="form-control" name="editarProducto[descripcion]" placeholder="Descripcion"><?php echo($productoEdit['descripcion']); ?></textarea>
                        </div>
                    </div>
                    <div class="box-footer">
                        <a href="/productos/" class="btn btn-default">Cancel</a>
                        <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-floppy-o"></i> Guardar</a></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>