<?php 
require_once ("database/productos.php");
$productosDB = new Productos;
$message = false;
if(isset($_POST['nuevoProducto']['nombre'], $_POST['nuevoProducto']['estado'])) {
    if($productosDB->create($_POST['nuevoProducto'])){
        echo '<script type="text/javascript">window.location.href = "/productos";</script>';
    }else {
        $message = true;
    }
}
?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Nuevo Producto</h3>
                </div>
                <form role="form" action="" method="post" name="formProducto">
                    <div class="box-body row">
                        <div class="form-group col-md-6">
                            <label for="nombre">Nombre</label>
                            <input type="text" class="form-control" name="nuevoProducto[nombre]" placeholder="Nombre" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Estado</label>
                            <select class="form-control" name="nuevoProducto[estado]">
                                <option value="1">Activo</option>
                                <option value="0">Inactivo</option>
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="descripcion">Descripcion</label>
                            <textarea rows="3"  class="form-control" name="nuevoProducto[descripcion]" placeholder="Descripcion"></textarea>
                        </div>
                    </div>
                    <div class="box-footer">
                        <a href="/productos/" class="btn btn-default">Cancel</a>
                        <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-floppy-o"></i> Guardar</a></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>