<?php 
if(isset($_GET['id'])){
    require_once ("database/clientes.php");
    $clientesDB = new Clientes;
    $result = $clientesDB->getID($_GET['id']);
    if($result->num_rows > 0){
        $clientePerfil = $result->fetch_assoc();
        // [ADD] Obtener la lista de referidos del usuario
        // $resultProduct = $clientesDB->getProductosNoReferidos($_GET['id']);
    }else{
        echo '<script type="text/javascript">window.location.href = "/clientes/lista";</script>';
    }
}else {
    echo '<script type="text/javascript">window.location.href = "/clientes/lista";</script>';
}
?>
<section class="content">
    <div class="row">
        <div class="col-md-3">
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <img class="profile-user-img img-responsive img-circle" src="/statics/img/cliente.png" alt="Imagen Cliente">
                        <?php 
                            echo '
                            <h5 class="profile-username text-center">'.$clientePerfil['primer_nombre'].' '.$clientePerfil['segundo_nombre'].' '.$clientePerfil['primer_apellido'].' '.$clientePerfil['segundo_apellido'].'</h5>
                            <p class="text-muted text-center">'.(($clientePerfil["estado"])? "<span class='label label-info'>Activo</span>": "<span class='label label-default'>Inactivo</span>").'</p>
                            <ul class="list-group list-group-unbordered">
                                <li class="list-group-item">
                                    <b>Fecha Nacimiento</b> <a class="pull-right">'.DateTime::createFromFormat('Y-m-d',$clientePerfil["fecha_nacimiento"])->format('d/m/Y').'</a>
                                </li>
                                <li class="list-group-item">
                                    <b>DPI</b> <a class="pull-right">'.$clientePerfil['dpi'].'</a>
                                </li>
                            </ul>
                            <a href="/clientes/editar?id='.$clientePerfil['id'].'" class="btn btn-primary btn-block"><b>Editar</b></a>
                            ';
                        
                        ?>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Información de Contacto</h3>
                </div>
                <div class="box-body">
                    <strong><i class="fa fa-phone"></i> Telefono/Celular</strong>
                    <p class="text-muted">
                        <?php echo 'Telefono: '.$clientePerfil['telefono'].'<br> Celular: '.$clientePerfil['celular']; ?>
                    </p>
                    <hr>
                    <strong><i class="fa fa-map-marker margin-r-5"></i> Direccion</strong>
                        <p class="text-muted"><?php echo $clientePerfil['direccion']; ?></p>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div id="perfilCliente">
                <input type="hidden" name="idClienteAJAX" value="<?php echo $clientePerfil['id']; ?>">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#actividad" data-toggle="tab">Actividad</a></li>
                        <li><a href="#bitacora" data-toggle="tab">Bitácora</a></li>
                    </ul>
                    <div class="tab-content" >
                        <div class="active tab-pane" id="actividad">
                            <div class="post">
                                <?php 
                                    if($clientePerfil["estado"] == 1){
                                ?>
                                    <div class="form-horizontal">
                                        <div class="form-group margin-bottom-none">
                                            <div class="col-sm-4">
                                                <select class="form-control input-sm" id="selectProducto" name="nuevoCliente[referencia]" v-model="dataReferencia.producto">
                                                    <option value="0" selected>Seleccionar Producto</option>
                                                    <option  v-for="(producto, index) in productos" v-cloak :value="producto.id" >{{ producto.nombre }}</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-5">
                                                <input class="form-control input-sm" placeholder="Notas de Referencias" v-model="dataReferencia.notas">
                                            </div>
                                            <div class="col-sm-3">
                                                <button type="button" class="btn btn-primary pull-right btn-block btn-sm" @click="addReferencia()" :disabled="dataReferencia.producto == 0">Agregar Referencia</button>
                                            </div>
                                        </div>
                                    </div>
                                <?php 
                                    };
                                ?>
                            </div>
                            <div class="row">
                                <ul class="timeline timeline-inverse">
                                    <li  v-for="(referencia, index) in referencias" v-cloak>
                                        <i v-if="referencia.estado == 1" class="fa fa-check bg-aqua" title="Activo" data-toggle="tooltip"></i>
                                        <i v-if="referencia.estado == 2" class="fa fa-handshake-o bg-olive" title="Realizado" data-toggle="tooltip"></i>
                                        <i v-if="referencia.estado == 3" class="fa fa-times bg-yellow" title="Inactivo" data-toggle="tooltip"></i>
                                        <div class="timeline-item">
                                            <span class="time"><i class="fa fa-clock-o"></i> {{ referencia.fecha }}</span>
                                            <h3 class="timeline-header">
                                                <a :href="'/usuarios/perfil?id='+referencia.id_usuario">{{ referencia.nombre_usuario }}</a>
                                                referenció el producto <a href="#">{{ referencia.nombre_producto }}</a>
                                                <div class="btn-group btn-group-xs" role="group">
                                                    <a href="" v-if="referencia.estado == 1" class="btn btn-default btn-sm" title="Realizado" data-toggle="tooltip" @click.prevent="confirmCambioEstadoReferencia(referencia.id, 2)"><i class="fa fa-handshake-o"></i></a> 
                                                    <a href="" v-if="referencia.estado == 1" class="btn btn-default btn-sm" title="Inactivar" data-toggle="tooltip" @click.prevent="confirmCambioEstadoReferencia(referencia.id, 3)"><i class="fa fa-times"></i></a> 
                                                    <a href="" v-if="referencia.estado > 1" class="btn btn-default btn-sm" title="Activar" data-toggle="tooltip" @click.prevent="confirmCambioEstadoReferencia(referencia.id, 1)"><i class="fa fa-check"></i></a> 
                                                    <a href="" class="btn btn-default btn-sm" title="Eliminar" data-toggle="tooltip" @click.prevent="confirmRemoveReferencia(referencia.id)"><i class="fa fa-trash"></i></a>
                                                </div> 
                                            </h3>
                                            <div class="timeline-body">
                                                {{ referencia.notas }}
                                                <p v-if="referencia.estado == 2">
                                                    <small class="text-muted">- Estado Realizado por <b>{{ referencia.nombre_estado }}</b>, comentarios: Ya se realizo la contratacion del producto por el cliente.</small>
                                                </p>
                                                <p v-if="referencia.estado == 3">
                                                    <small class="text-muted">- Estado Inactivado por <b>Juan Perez</b> por el motivo: El cliente a indicado otro que otra persona lo refirio.</small>
                                                </p>
                                            </div>
                                        </div>
                                    </li>
                                    <li v-if="referencias.length <= 0">
                                        <i class="fa fa-clock-o bg-gray"></i>
                                        <div class="timeline-item">
                                            <div class="timeline-body">
                                               Sin referencia registrada.
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="tab-pane" id="bitacora">
                            <div class="row" style="padding-bottom: 20px;">
                                <?php 
                                    if($clientePerfil["estado"] == 1){
                                ?>
                                <form class="form-horizontal col-sm-12">
                                    <div class="form-group margin-bottom-none">
                                        <div class="col-sm-9">
                                            <input class="form-control input-sm" placeholder="Comentario" v-model="comentarios">
                                        </div>
                                        <div class="col-sm-3">
                                            <button type="button" class="btn btn-primary pull-right btn-block btn-sm" :disabled="comentarios == ''" @click="addBitacora()">Agregar</button>
                                        </div>
                                    </div>
                                </form>
                                <?php 
                                    };
                                ?>
                            </div>
                            <div class="row">
                                <ul class="timeline timeline-inverse">
                                    <li  v-for="(bitacora, index) in bitacoras">
                                        <i class="fa fa-comments bg-yellow"></i>
                                        <div class="timeline-item">
                                            <span class="time"><i class="fa fa-clock-o"></i> {{ bitacora.fecha }}</span>
                                            <h3 class="timeline-header">
                                                <a :href="'/usuarios/perfil?id='+bitacora.id_usuario">{{ bitacora.nombre_usuario }}</a>
                                                <div class="btn-group btn-group-xs" role="group">
                                                    <a href="" class="btn btn-default btn-sm" title="Eliminar" data-toggle="tooltip" @click.prevent="confirmRemoveBitacora(bitacora.id)"><i class="fa fa-trash"></i></a>
                                                </div> 
                                            </h3>
                                            <div class="timeline-body">{{ bitacora.comentario }}</div>
                                        </div>
                                    </li>
                                    <li v-if="bitacoras.length <= 0">
                                        <div class="timeline-item">
                                            <div class="timeline-body">
                                               No se ha registrado ningun dato.
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <i class="fa fa-clock-o bg-gray"></i>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- MODALS -->
                <div class="modal modal-warning fade" id="modalCambioEstadoReferencia">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Cofirmar Cambio de Estado</h4>
                            </div>
                            <div class="modal-body">
                                <p>¿Esta seguro de cambiar el estado a la referencia?</p>
                                <p >
                                    <input type="text" class="form-control" placeholder="Comentario del Cambio de Estado" v-model="comentarioEstadoReferencia">
                                </p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
                                <button type="button" class="btn btn-outline" @click.prevent="cambiarEstadoReferencia()" :disabled="comentarioEstadoReferencia == ''">Cambiar Estado</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal modal-danger fade" id="modalRemoveReferencia">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Confirmar Eliminacion</h4>
                            </div>
                            <div class="modal-body">
                                <p>¿Esta seguro de Eliminar la Referencia?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
                                <a class="btn btn-outline" @click.prevent="removeReferencia()">Eliminar</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal modal-danger fade" id="modalRemoveBitacora">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Confirmar Eliminacion</h4>
                            </div>
                            <div class="modal-body">
                                <p>¿Esta seguro de Eliminar el comentario de la Bitacora?</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Cerrar</button>
                                <a class="btn btn-outline" @click.prevent="removeBitacora()">Eliminar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>