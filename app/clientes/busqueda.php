<?php 
require_once ("database/clientes.php");
$clientesDB = new Clientes;

// $clientes = $clientesDB->getAll();
$showResultdo = false;

if(isset($_GET['buscarCliente']['primer_nombre'], $_GET['buscarCliente']['primer_apellido'])) {
    $respCliente = $clientesDB->buscarCliente($_GET['buscarCliente']);

    if($respCliente->num_rows > 0){
        $showResultdo = true;
    }
    // $respCliente = $result->fetch_array();
    // var_dump($respCliente);
    // if($respCliente['code'] == 1){
    //     // echo '<script type="text/javascript">window.location.href = "/clientes/lista";</script>';
    //     echo '<script type="text/javascript">window.location.href = "/clientes/perfil?id='.$respCliente['data']['id'].'";</script>';
    // }
}

?>

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Buscar Cliente</h3>
                </div>
                <form role="form" action="" method="get" name="formCliente">
                    <?php 
                        if(isset($_POST['buscarCliente']['primer_nombre'], $_POST['buscarCliente']['primer_apellido'], $_POST['buscarCliente']['fecha_nacimiento'])) {
                            if($respCliente['code'] == 2){
                                echo '
                                <div class="pad margin">
                                    <div class="callout callout-warning" style="margin-bottom: 0!important;">
                                        El cliente <b>'.$respCliente['data']['primer_nombre'].' '.$respCliente['data']['primer_apellido'].'</b> ya esta registrado. 
                                        <a href="/clientes/perfil?id='.$respCliente['data']['id'].'" class="btn btn-warning"><i class="fa fa-address-book"></i> Ver Cliente</a>
                                    </div>
                                </div>';
                            }else if($respCliente['code'] == 3){
                                echo '
                                <div class="pad margin">
                                    <div class="callout callout-warning" style="margin-bottom: 0!important;">
                                        No se ha podido guardar la referencia, puedes ir al perfil del cliente <b>'.$respCliente['data']['primer_nombre'].' '.$respCliente['data']['primer_apellido'].'</b> para agregarla de nuevo. 
                                        <a href="/clientes/perfil?id='.$respCliente['data']['id'].'" class="btn btn-warning"><i class="fa fa-address-book"></i> Ver Cliente</a>
                                    </div>
                                </div>';
                            }else {
                                echo '
                                <div class="pad margin">
                                    <div class="callout callout-danger" style="margin-bottom: 0!important;">
                                        A ocurrido un error, favor comunicate con el administrador del sistema.
                                    </div>
                                </div>';
                            }
                        }
                    ?>
                    
                    <div class="box-body row">
                        <div class="form-group col-md-6">
                            <label>Primer Nombre <small class="text-muted">(Campo Obligatorio)</small></label>
                            <input type="text" class="form-control" name="buscarCliente[primer_nombre]" placeholder="Primer Nombre" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Segundo Nombre</label>
                            <input type="text" class="form-control" name="buscarCliente[segundo_nombre]" placeholder="Segundo Nombre" >
                        </div>
                        <div class="form-group col-md-6">
                            <label>Primer Apellido <small class="text-muted">(Campo Obligatorio)</small></label>
                            <input type="text" class="form-control" name="buscarCliente[primer_apellido]" placeholder="Primer Apellido" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Segundo Apellido</label>
                            <input type="text" class="form-control" name="buscarCliente[segundo_apellido]" placeholder="Segundo Apellido" >
                        </div>
                        <div class="form-group col-md-6">
                            <label>Fecha de Nacimiento</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" name="buscarCliente[fecha_nacimiento]">
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label>DPI</label>
                            <input type="number" class="form-control" name="buscarCliente[dpi]" placeholder="DPI" >
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-search"></i> Buscar Cliente</a></button>
                    </div>
                </form>
            </div>
        </div>
        <?php 
            if($showResultdo) {
        ?>
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Resultado</h3>
                </div>
                <div class="box-body">
                    <table class="table table-bordered table-striped tableData">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nombres</th>
                                <th>Apellidos</th>
                                <th>Fecha Nacimiento</th>
                                <th>DPI</th>
                                <th>Telefono - Celular</th>
                                <th>Estado</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            foreach($respCliente as $cliente) {
                                $estado = ($cliente["estado"])? "<span class='label label-info'>Activo</span>": "<span class='label label-default'>Inactivo</span>";
                                echo '
                                    <tr>
                                        <td>'.$cliente["id"].'</td>
                                        <td><a href="/clientes/perfil?id='.$cliente["id"].'">'.$cliente["primer_nombre"].' '.$cliente["segundo_nombre"].'</a></td>
                                        <td>'.$cliente["primer_apellido"].' '.$cliente["segundo_apellido"].'</td>
                                        <td>'.DateTime::createFromFormat('Y-m-d',$cliente["fecha_nacimiento"])->format('d/m/Y').'</td>
                                        <td>'.$cliente["dpi"].'</td>
                                        <td>'.$cliente["telefono"].' - '.$cliente["celular"].'</td>
                                        <td>'.$estado.'</td>
                                        <td>
                                            <div class="btn-group">
                                                <a href="/clientes/perfil?id='.$cliente["id"].'" title="Ver" class="btn bg-olive btn-flat" data-toggle="tooltip"><i class="fa fa-eye"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                    ';
                            }
                            
                            ?>
                    
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <?php 
            };
        ?>
    </div>

</section>