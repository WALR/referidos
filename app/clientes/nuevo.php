<?php 
require_once ("database/productos.php");
$productosDB = new Productos;
$productos = $productosDB->getAll();

require_once ("database/clientes.php");
$clientesDB = new Clientes;

if(isset($_POST['nuevoCliente']['primer_nombre'], $_POST['nuevoCliente']['primer_apellido'], $_POST['nuevoCliente']['fecha_nacimiento'])) {
    $respCliente = $clientesDB->create($_POST['nuevoCliente']);
    if($respCliente['code'] == 1){
        // echo '<script type="text/javascript">window.location.href = "/clientes/lista";</script>';
        echo '<script type="text/javascript">window.location.href = "/clientes/perfil?id='.$respCliente['data']['id'].'";</script>';
    }
}
?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Nuevo Cliente</h3>
                </div>
                <form role="form" action="" method="post" name="formCliente">
                    <?php 
                        if(isset($_POST['nuevoCliente']['primer_nombre'], $_POST['nuevoCliente']['primer_apellido'], $_POST['nuevoCliente']['fecha_nacimiento'])) {
                            if($respCliente['code'] == 2){
                                echo '
                                <div class="pad margin">
                                    <div class="callout callout-warning" style="margin-bottom: 0!important;">
                                        El cliente <b>'.$respCliente['data']['primer_nombre'].' '.$respCliente['data']['primer_apellido'].'</b> ya esta registrado. 
                                        <a href="/clientes/perfil?id='.$respCliente['data']['id'].'" class="btn btn-warning"><i class="fa fa-address-book"></i> Ver Cliente</a>
                                    </div>
                                </div>';
                            }else if($respCliente['code'] == 3){
                                echo '
                                <div class="pad margin">
                                    <div class="callout callout-warning" style="margin-bottom: 0!important;">
                                        No se ha podido guardar la referencia, puedes ir al perfil del cliente <b>'.$respCliente['data']['primer_nombre'].' '.$respCliente['data']['primer_apellido'].'</b> para agregarla de nuevo. 
                                        <a href="/clientes/perfil?id='.$respCliente['data']['id'].'" class="btn btn-warning"><i class="fa fa-address-book"></i> Ver Cliente</a>
                                    </div>
                                </div>';
                            }else {
                                echo '
                                <div class="pad margin">
                                    <div class="callout callout-danger" style="margin-bottom: 0!important;">
                                        A ocurrido un error, favor comunicate con el administrador del sistema.
                                    </div>
                                </div>';
                            }
                        }
                    ?>
                    
                    <div class="box-body row">
                        <h5 class="col-md-12"><i class="fa fa-angle-double-right text-primary"></i> Información Personal</h5>
                        <div class="form-group col-md-6">
                            <label>Primer Nombre <small class="text-muted">(Campo Obligatorio)</small></label>
                            <input type="text" class="form-control" name="nuevoCliente[primer_nombre]" placeholder="Primer Nombre" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Segundo Nombre</label>
                            <input type="text" class="form-control" name="nuevoCliente[segundo_nombre]" placeholder="Segundo Nombre" >
                        </div>
                        <div class="form-group col-md-6">
                            <label>Primer Apellido <small class="text-muted">(Campo Obligatorio)</small></label>
                            <input type="text" class="form-control" name="nuevoCliente[primer_apellido]" placeholder="Primer Apellido" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Segundo Apellido</label>
                            <input type="text" class="form-control" name="nuevoCliente[segundo_apellido]" placeholder="Segundo Apellido" >
                        </div>
                        <div class="form-group col-md-6">
                            <label>Fecha de Nacimiento <small class="text-muted">(Campo Obligatorio)</small></label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" name="nuevoCliente[fecha_nacimiento]" required>
                            </div>
                            <!-- <input type="text" class="form-control" name="nuevoCliente[fecha_nacimiento]" placeholder="Fecha de Nacimiento" required> -->
                        </div>
                        <div class="form-group col-md-6">
                            <label>DPI</label>
                            <input type="number" class="form-control" name="nuevoCliente[dpi]" placeholder="DPI" >
                        </div>
                        <h5 class="col-md-12"><i class="fa fa-angle-double-right text-primary"></i> Información de Contacto</h5>
                        <div class="form-group col-md-6">
                            <label>Telefono</label>
                            <input type="number" class="form-control" name="nuevoCliente[telefono]" placeholder="Telefono" >
                        </div>
                        <div class="form-group col-md-6">
                            <label>Celular</label>
                            <input type="number" class="form-control" name="nuevoCliente[celular]" placeholder="Celular" >
                        </div>
                        <div class="form-group col-md-12">
                            <label>Direccion</label>
                            <input type="text" class="form-control" name="nuevoCliente[direccion]" placeholder="Direccion" >
                        </div>
                        <h5 class="col-md-12"><i class="fa fa-angle-double-right text-primary"></i> Referencia de Producto</h5>
                        <div class="form-group col-md-12">
                            <label>Productos</label>
                            <select class="form-control" name="nuevoCliente[referencia]">
                                <option value="0">Seleccionar Producto</option>
                                <?php 
                                    foreach($productos as $producto) {
                                        echo "<option value='".$producto["id"]."'>".$producto["nombre"]."</option>";
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="Notas">Notas</label>
                            <textarea rows="2"  class="form-control" name="nuevoCliente[notas]" placeholder="Notas"></textarea>
                        </div>
                    </div>
                    <div class="box-footer">
                        <!-- <a href="/clientes/lista" class="btn btn-default">Cancel</a> -->
                        <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-floppy-o"></i> Guardar</a></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- <div class="row">
        <div class="pad margin">
            <div class="callout callout-info" style="margin-bottom: 0!important;">
                <h4><i class="fa fa-info"></i> Nota:</h4>
                Es recomendable buscar si el cliente ya fue registrado.
                <a href="/clientes/buscar/" class="btn btn-info"><i class="fa fa-search"></i> Buscar Cliente</a>
            </div>
        </div>
    </div> -->
</section>