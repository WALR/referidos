<?php 
if(isset($_GET['id'])){
    require_once ("database/clientes.php");
    $clientesDB = new Clientes;
    $message = false;

    $result = $clientesDB->getID($_GET['id']);
    if($result->num_rows > 0){
        $clienteEdit = $result->fetch_assoc();
        if(isset($_POST['editarCliente']['primer_nombre'], $_POST['editarCliente']['primer_apellido'], $_POST['editarCliente']['fecha_nacimiento'])) {
            if($clientesDB->edit($clienteEdit['id'], $_POST['editarCliente'])){
                echo '<script type="text/javascript">window.location.href = "/clientes/perfil?id='.$clienteEdit['id'].'";</script>';
            }else {
                $message = true;
            }
        }

    }else{
        echo '<script type="text/javascript">window.location.href = "/clientes/lista";</script>';
    }
}else {
    echo '<script type="text/javascript">window.location.href = "/clientes/lista";</script>';
}
?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Editar Cliente</h3>
                </div>
                <form role="form" action="" method="post" name="formCliente">
                    <div class="box-body row">
                        <?php 
                            if($message){
                                echo '
                                <div class="pad margin">
                                    <div class="callout callout-danger" style="margin-bottom: 0!important;">
                                    A ocurrido un error, favor comunicate con el administrador del sistema.
                                    </div>
                                </div>
                                ';
                            }
                        ?>
                        <h5 class="col-md-12"><i class="fa fa-angle-double-right text-primary"></i> Información Personal</h5>
                        <div class="form-group col-md-6">
                            <label>Primer Nombre <small class="text-muted">(Campo Obligatorio)</small></label>
                            <input type="text" class="form-control" name="editarCliente[primer_nombre]" placeholder="Primer Nombre" value="<?php echo($clienteEdit['primer_nombre']) ?>" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Segundo Nombre</label>
                            <input type="text" class="form-control" name="editarCliente[segundo_nombre]" placeholder="Segundo Nombre" value="<?php echo($clienteEdit['segundo_nombre']) ?>" >
                        </div>
                        <div class="form-group col-md-6">
                            <label>Primer Apellido <small class="text-muted">(Campo Obligatorio)</small></label>
                            <input type="text" class="form-control" name="editarCliente[primer_apellido]" placeholder="Primer Apellido" value="<?php echo($clienteEdit['primer_apellido']) ?>" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Segundo Apellido</label>
                            <input type="text" class="form-control" name="editarCliente[segundo_apellido]" placeholder="Segundo Apellido" value="<?php echo($clienteEdit['segundo_apellido']) ?>" >
                        </div>
                        <div class="form-group col-md-6">
                            <label>Fecha de Nacimiento <small class="text-muted">(Campo Obligatorio)</small></label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'" data-mask="" name="editarCliente[fecha_nacimiento]" value="<?php echo(DateTime::createFromFormat('Y-m-d', $clienteEdit['fecha_nacimiento'])->format('d/m/Y')) ?>" required>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label>DPI</label>
                            <input type="number" class="form-control" name="editarCliente[dpi]" placeholder="DPI" value="<?php echo($clienteEdit['dpi']) ?>">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Estado</label>
                            <select class="form-control" name="editarCliente[estado]">
                                <option value="1">Activo</option>
                                <option value="0">Inactivo</option>
                            </select>
                        </div>
                        <h5 class="col-md-12"><i class="fa fa-angle-double-right text-primary"></i> Información de Contacto</h5>
                        <div class="form-group col-md-6">
                            <label>Telefono</label>
                            <input type="number" class="form-control" name="editarCliente[telefono]" placeholder="Telefono" value="<?php echo($clienteEdit['telefono']) ?>">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Celular</label>
                            <input type="number" class="form-control" name="editarCliente[celular]" placeholder="Celular" value="<?php echo($clienteEdit['celular']) ?>">
                        </div>
                        <div class="form-group col-md-12">
                            <label>Direccion</label>
                            <input type="text" class="form-control" name="editarCliente[direccion]" placeholder="Direccion" value="<?php echo($clienteEdit['direccion']) ?>">
                        </div>
                    </div>
                    <div class="box-footer">
                        <a href="/clientes/lista" class="btn btn-default">Cancel</a>
                        <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-floppy-o"></i> Guardar</a></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>