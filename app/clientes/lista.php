<?php 
require_once ("database/clientes.php");
$clientesDB = new Clientes;
$clientes = $clientesDB->getAll();

?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Clientes</h3>
                    <div class="pull-right btn-control">
                        <a href="/clientes/nuevo/" class="btn btn-block btn-primary">
                            <i class="fa fa-plus-square"></i>
                            Agregar Cliente
                        </a>
                    </div>

                </div>
                <div class="box-body">
                    <table class="table table-bordered table-striped tableData">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nombres</th>
                                <th>Apellidos</th>
                                <th>Fecha Nacimiento</th>
                                <th>DPI</th>
                                <th>Telefono - Celular</th>
                                <th>Estado</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            foreach($clientes as $cliente) {
                                $estado = ($cliente["estado"])? "<span class='label label-info'>Activo</span>": "<span class='label label-default'>Inactivo</span>";
                                echo '
                                    <tr>
                                        <td>'.$cliente["id"].'</td>
                                        <td><a href="/clientes/perfil?id='.$cliente["id"].'">'.$cliente["primer_nombre"].' '.$cliente["segundo_nombre"].'</a></td>
                                        <td>'.$cliente["primer_apellido"].' '.$cliente["segundo_apellido"].'</td>
                                        <td>'.DateTime::createFromFormat('Y-m-d',$cliente["fecha_nacimiento"])->format('d/m/Y').'</td>
                                        <td>'.$cliente["dpi"].'</td>
                                        <td>'.$cliente["telefono"].' - '.$cliente["celular"].'</td>
                                        <td>'.$estado.'</td>
                                        <td>
                                            <div class="btn-group">
                                                <a href="/clientes/perfil?id='.$cliente["id"].'" title="Ver" class="btn bg-olive btn-flat" data-toggle="tooltip"><i class="fa fa-eye"></i></a>
                                                <a href="/clientes/editar?id='.$cliente["id"].'" title="Editar" class="btn bg-navy btn-flat" data-toggle="tooltip"><i class="fa fa-edit"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                    ';
                            }
                            
                            ?>
                    
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>