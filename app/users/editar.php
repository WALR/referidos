<?php 
if(isset($_GET['id'])){
    require_once ("database/users.php");
    $usuariosDB = new Users;
    $result = $usuariosDB->getID($_GET['id']);
    if($result->num_rows > 0){
        $userEdit = $result->fetch_assoc();

        if(isset($_POST['editarUsuario']['nombre'], $_POST['editarUsuario']['apellido'], $_POST['editarUsuario']['usuario'])) {
            if($usuariosDB->edit($userEdit['id'], $_POST['editarUsuario'])){
                echo '<script type="text/javascript">window.location.href = "/usuarios";</script>';
            }else {
                $message = true;
            }
        }

    }else{
        echo '<script type="text/javascript">window.location.href = "/usuarios";</script>';
    }
}else {
    echo '<script type="text/javascript">window.location.href = "/usuarios";</script>';
}
?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Editar Usuarios</h3>
                </div>
                <form role="form" action="" method="post" name="formUsuario">
                    <div class="box-body row">
                        <div class="form-group col-md-6">
                            <label for="nombre">Nombre</label>
                            <input type="text" class="form-control" name="editarUsuario[nombre]" placeholder="Nombre" value="<?php echo($userEdit['nombre']); ?>" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="apellido">Apellido</label>
                            <input type="text" class="form-control" name="editarUsuario[apellido]" placeholder="Apellido" value="<?php echo($userEdit['apellido']); ?>" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="usuario">Usuario</label>
                            <input type="text" class="form-control" name="editarUsuario[usuario]" placeholder="Usuario" value="<?php echo($userEdit['username']); ?>" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="password">Contraseña</label>
                            <input type="password" class="form-control" name="editarUsuario[password]" placeholder="Contraseña">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="numero_empleado">Numero Empleado</label>
                            <input type="number" class="form-control" name="editarUsuario[numero_empleado]" value="<?php echo($userEdit['numero_empleado']); ?>" placeholder="Numero Empleado">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Rol</label>
                            <select class="form-control" name="editarUsuario[rol]">
                                <option value="0" <?php echo(($userEdit['admin'])? '':'selected'); ?>>Usuario</option>
                                <option value="1" <?php echo(($userEdit['admin'])? 'selected': ''); ?>>Administrador</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Estado</label>
                            <select class="form-control" name="editarUsuario[estado]">
                                <option value="1" <?php echo(($userEdit['estado'])? 'selected' : ''); ?>>Activo</option>
                                <option value="0" <?php echo(($userEdit['estado'])? '': 'selected'); ?>>Inactivo</option>
                            </select>
                        </div>
                    </div>
                    <div class="box-footer">
                        <a href="/usuarios/" class="btn btn-default">Cancel</a>
                        <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-floppy-o"></i> Guardar</a></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>