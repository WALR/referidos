<?php 
if(isset($_SESSION['login'])){
    require_once ("database/users.php");
    $msgNuevPass = false;
    $msgActualPass = false;
    $usuariosDB = new Users;
    $result = $usuariosDB->getID($_SESSION['id']);
    if($result->num_rows > 0){
        $userMe = $result->fetch_assoc();
        
        if(isset($_POST['miPass']['passwordActual'], $_POST['miPass']['passwordNueva'], $_POST['miPass']['passwordNuevaR'])) {
            
            if($_POST['miPass']['passwordNueva'] !== $_POST['miPass']['passwordNuevaR']){
                $msgNuevPass = true;
            }else {

                if($usuariosDB->editPassword($_SESSION['id'], $_POST['miPass']['passwordActual'], $_POST['miPass']['passwordNueva'])){
                    echo '<script type="text/javascript">window.location.href = "/logout";</script>';
                }else {
                    $msgActualPass = true;
                }
            }
        }

    }else{
        echo '<script type="text/javascript">window.location.href = "/";</script>';
    }
}else {
    echo '<script type="text/javascript">window.location.href = "/";</script>';
}
?>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <img class="profile-user-img img-responsive img-circle" src="/statics/img/user.png" alt="Perfil Usuario">
                    <h3 class="profile-username text-center"><?php echo($userMe['nombre'].' '.$userMe['apellido']); ?></h3>
                    <p class="text-muted text-center"><?php echo($userMe['username']); ?></p>
                    <ul class="list-group list-group-unbordered col-md-6">
                        <li class="list-group-item">
                            <b>Nombre:</b> <a class="pull-right"><?php echo($userMe['nombre']); ?></a>
                        </li>
                        <li class="list-group-item">
                            <b>Usuario:</b> <a class="pull-right"><?php echo($userMe['username']); ?></a>
                        </li>
                        <li class="list-group-item">
                            <b>Rol:</b> <a class="pull-right"><?php echo(($userMe['admin'])? 'Administrador':'Usuario'); ?></a>
                        </li>
                    </ul>
                    <ul class="list-group list-group-unbordered col-md-6">
                        <li class="list-group-item">
                            <b>Apellido:</b> <a class="pull-right"><?php echo($userMe['apellido']); ?></a>
                        </li>
                        <li class="list-group-item">
                            <b>Numero de Empleado:</b> <a class="pull-right"><?php echo($userMe['numero_empleado']); ?></a>
                        </li>
                        <li class="list-group-item">
                            <b>Estado:</b> <a class="pull-right"><?php echo(($userMe['estado'])? 'Activo':'Inactivo'); ?></a>
                        </li>
                    </ul>

                    <!-- <a href="/usuarios/editar?id=<?php echo($userMe['id']); ?>" class="col-md-6 btn btn-primary btn-block"><b>Editar</b></a> -->
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Contraseña</h3>
                </div>
                <form role="form" action="" method="post" name="formUsuario">
                <div class="box-body">
                    <?php 
                        if($msgNuevPass){
                            echo '
                            <div class="alert alert-danger alert-error col-md-12">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <h5>Las contraseñas nuevas ingresadas no coinciden.</h5>
                            </div>';
                        }
                        if($msgActualPass){
                            echo '
                            <div class="alert alert-danger alert-error col-md-12">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <h5>Tu contraseña actual ingresada es incorrecta.</h5>
                            </div>';
                        }
                    ?>
                    <div class="form-group col-md-12">
                        <label for="password">Contraseña Actual</label>
                        <input type="password" class="form-control" name="miPass[passwordActual]" placeholder="Contraseña Actual" required>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="password">Nueva Contraseña</label>
                        <input type="password" class="form-control" name="miPass[passwordNueva]" placeholder="Nueva Contraseña" required>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="password">Repite Nueva Contraseña</label>
                        <input type="password" class="form-control" name="miPass[passwordNuevaR]" placeholder="Repite Nueva Contraseña" required>
                    </div>
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-floppy-o"></i> Guardar</a></button>
                </div>
            </div>
            </form>
        </div>
    </div>
</section>