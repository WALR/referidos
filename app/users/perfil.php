<?php 
if(isset($_GET['id'])){
    require_once ("database/users.php");
    $usuariosDB = new Users;
    $result = $usuariosDB->getID($_GET['id']);
    if($result->num_rows > 0){
        $userPerfil = $result->fetch_assoc();

        // [ADD] Obtener la lista de referidos del usuario
        
        $respCliente =$usuariosDB->getReferidosUsuario($_GET['id']);
    }else{
        echo '<script type="text/javascript">window.location.href = "/usuarios";</script>';
    }
}else {
    echo '<script type="text/javascript">window.location.href = "/usuarios";</script>';
}
?>
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header">
                        <div class="pull-right btn-control">
                            <a href="/usuarios/editar?id=<?php echo($userPerfil['id']); ?>" class="btn btn-link">
                                <i class="fa fa-edit"></i>
                                Editar Usuario
                            </a>
                        </div>

                    </div>
                    <div class="box-body box-profile">
                        <img class="profile-user-img img-responsive img-circle" src="/statics/img/user.png" alt="Perfil Usuario">
                        <h3 class="profile-username text-center"><?php echo($userPerfil['nombre'].' '.$userPerfil['apellido']); ?></h3>
                        <p class="text-muted text-center"><?php echo($userPerfil['username']); ?></p>
                        <ul class="list-group list-group-unbordered col-md-6">
                            <li class="list-group-item">
                                <b>Nombre:</b> <a class="pull-right"><?php echo($userPerfil['nombre']); ?></a>
                            </li>
                            <li class="list-group-item">
                                <b>Usuario:</b> <a class="pull-right"><?php echo($userPerfil['username']); ?></a>
                            </li>
                            <li class="list-group-item">
                                <b>Rol:</b> <a class="pull-right"><?php echo(($userPerfil['admin'])? 'Administrador':'Usuario'); ?></a>
                            </li>
                        </ul>
                        <ul class="list-group list-group-unbordered col-md-6">
                            <li class="list-group-item">
                                <b>Apellido:</b> <a class="pull-right"><?php echo($userPerfil['apellido']); ?></a>
                            </li>
                            <li class="list-group-item">
                                <b>Numero de Empleado:</b> <a class="pull-right"><?php echo($userPerfil['numero_empleado']); ?></a>
                            </li>
                            <li class="list-group-item">
                                <b>Estado:</b> <a class="pull-right"><?php echo(($userPerfil['estado'])? 'Activo':'Inactivo'); ?></a>
                            </li>
                        </ul>

                        <!-- <a href="/usuarios/editar?id=<?php echo($userPerfil['id']); ?>" class="col-md-6 btn btn-primary btn-block"><b>Editar</b></a> -->
                    </div>
                </div>
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Referidos</h3>
                </div>
                <div class="box-body">
                    <table class="table table-bordered table-striped tableData">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nombres</th>
                                <th>Apellidos</th>
                                <th>Fecha Nacimiento</th>
                                <th>DPI</th>
                                <th>Telefono - Celular</th>
                                <th>Estado</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            foreach($respCliente as $cliente) {
                                $estado = ($cliente["estado"])? "<span class='label label-info'>Activo</span>": "<span class='label label-default'>Inactivo</span>";
                                echo '
                                    <tr>
                                        <td>'.$cliente["id"].'</td>
                                        <td><a href="/clientes/perfil?id='.$cliente["id"].'">'.$cliente["primer_nombre"].' '.$cliente["segundo_nombre"].'</a></td>
                                        <td>'.$cliente["primer_apellido"].' '.$cliente["segundo_apellido"].'</td>
                                        <td>'.DateTime::createFromFormat('Y-m-d',$cliente["fecha_nacimiento"])->format('d/m/Y').'</td>
                                        <td>'.$cliente["dpi"].'</td>
                                        <td>'.$cliente["telefono"].' - '.$cliente["celular"].'</td>
                                        <td>'.$estado.'</td>
                                        <td>
                                            <div class="btn-group">
                                                <a href="/clientes/perfil?id='.$cliente["id"].'" title="Ver" class="btn bg-olive btn-flat" data-toggle="tooltip"><i class="fa fa-eye"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                    ';
                            }
                            
                            ?>
                    
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>