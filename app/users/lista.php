<?php 
require_once ("database/users.php");
$usuariosDB = new Users;
$usuarios = $usuariosDB->getAll();

?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Usuarios</h3>
                    <div class="pull-right btn-control">
                        <a href="/usuarios/nuevo/" class="btn btn-block btn-primary">
                            <i class="fa fa-plus-square"></i>
                            Nuevo Usuario
                        </a>
                    </div>

                </div>
                <div class="box-body">
                    <table class="table table-bordered table-striped tableData">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Usuario</th>
                                <th>Nombre y Apellido</th>
                                <th>Numero de Empleado</th>
                                <th>Rol</th>
                                <th>Estado</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            foreach($usuarios as $usuario) {
                                // print_r($usuario["id"]);
                                $rol = ($usuario["admin"])? "<span class='label label-primary'>Administrador</span>": "<span class='label label-info'>Usuario</span>";
                                $estado = ($usuario["estado"])? "<span class='label label-info'>Activo</span>": "<span class='label label-default'>Inactivo</span>";
                                echo '
                                    <tr>
                                        <td>'.$usuario["id"].'</td>
                                        <td><a href="/usuarios/perfil?id='.$usuario["id"].'">'.$usuario["username"].'</a></td>
                                        <td>'.$usuario["nombre"].' '.$usuario["apellido"].'</td>
                                        <td>'.$usuario["numero_empleado"].'</td>
                                        <td>'.$rol.'</td>
                                        <td>'.$estado.'</td>
                                        <td>
                                            <div class="btn-group">
                                                <a href="/usuarios/perfil?id='.$usuario["id"].'" title="Ver" class="btn bg-olive btn-flat" data-toggle="tooltip"><i class="fa fa-eye"></i></a>
                                                <a href="/usuarios/editar?id='.$usuario["id"].'" title="Editar" class="btn bg-navy btn-flat" data-toggle="tooltip"><i class="fa fa-edit"></i></a>
                                            </div>
                                        </td>
                                    </tr>
                                    ';
                            }
                            
                            ?>
                    
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>