<?php 

$request_uri = explode('?', $_SERVER['REQUEST_URI'], 2);
$request_uri = trim( $request_uri[0], "/" );
$request_uri = explode( "/", $request_uri );

if(sizeOf($request_uri)>1){
    switch ($request_uri[1]) {
        case '':
            require 'app/users/lista.php';
            break;
        case 'nuevo':
            require 'app/users/nuevo.php';
            break;
        case 'editar':
            require 'app/users/editar.php';
            break;
        case 'perfil':
            require 'app/users/perfil.php';
            break;
        case 'mi-perfil':
            require 'app/users/miPerfil.php';
            break;
        case 'mis-referidos':
            require 'app/users/misReferidos.php';
            break;
        default:
            require 'app/users/lista.php';
            break;
    }
}else {
    require 'app/users/lista.php';
}

?>