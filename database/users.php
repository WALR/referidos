<?php 
require_once ("conection.php");
class Users{
	private $conection;
    private $nameTable;

    public function __construct(){
        $this->conection = new Conection;
        $this->nameTable = "usuario";
    }

    public function getAll(){
        $db = $this->conection->initConection();
        $query = "SELECT id, username, nombre, apellido, numero_empleado, admin, estado FROM {$this->nameTable}";
        return $this->conection->runquery($db, $query);
    }

    public function getID($id){
        $db = $this->conection->initConection();
        $query = "SELECT id, username, password, nombre, apellido, numero_empleado, admin, estado FROM ".$this->nameTable." WHERE id = ".$id.";";
        return $this->conection->runquery($db, $query);
    }

    public function create($data){
        $db = $this->conection->initConection();
        $query = "INSERT INTO ".$this->nameTable." (username, password, nombre, apellido, numero_empleado, admin, estado) 
                VALUES ('".$data['usuario']."', '".password_hash($data['password'], PASSWORD_DEFAULT)."', '".$data['nombre']."', 
                '".$data['apellido']."', ".$data['numero_empleado'].", ".$data['rol'].", ".$data['estado'].");";
        return $this->conection->runquery($db, $query);
    }

    public function edit($id, $data){
        $db = $this->conection->initConection();
        $pass = (!empty($data['password']))? ",password =  '".password_hash($data['password'], PASSWORD_DEFAULT)."'": "";
        $query = "UPDATE ".$this->nameTable." SET username = '".$data['usuario']."', nombre = '".$data['nombre']."', 
                apellido = '".$data['apellido']."', numero_empleado = ".$data['numero_empleado'].", admin = ".$data['rol'].", 
                estado = ".$data['estado']." ".$pass."  WHERE id = ".$id.";";
        return $this->conection->runquery($db, $query);
    }
    
    public function editPassword($id, $actualPass, $nuevoPass){
        $db = $this->conection->initConection();
        $result = $this->getID($id);
        $user = $result->fetch_assoc();
        if(password_verify($actualPass, $user['password'])){
            $query = "UPDATE ".$this->nameTable." SET password = '".password_hash($nuevoPass, PASSWORD_DEFAULT)."' WHERE id = ".$id.";";
            return $this->conection->runquery($db, $query);
        } else {
            return false;
        }
    }

    public function delete(){

    }

    public function login($user, $pass){
        $db = $this->conection->initConection();
        $query = "SELECT id, username, password, nombre, apellido, numero_empleado, admin, estado FROM ".$this->nameTable." 
                WHERE username = '".$user."';";
        $result =  $this->conection->runquery($db, $query);
        if($result->num_rows > 0) {
            $user = $result->fetch_assoc();
            if(password_verify($pass, $user['password'])){
                $_SESSION['login'] = True;
                $_SESSION['id'] = $user['id'];
                $_SESSION['username'] = $user['username'];
                $_SESSION['nombre'] = $user['nombre'];
                $_SESSION['apellido'] = $user['apellido'];
                $_SESSION['numero_empleado'] = $user['numero_empleado'];
                $_SESSION['admin'] = $user['admin'];
                
                return true;
            }else {
                return false;
            }
        }else {
            return false;
        }
    }

    public function getReferidosUsuario($idUsuario){
        $db = $this->conection->initConection();
        $query ="SELECT c.id, c.primer_nombre, c.segundo_nombre, c.primer_apellido, c.segundo_apellido,
                c.fecha_nacimiento, c.dpi, c.telefono, c.celular, c.estado 
                FROM referencia r INNER JOIN cliente c ON c.id = r.id_cliente 
                WHERE id_usuario =".$idUsuario." GROUP BY c.id;";
        // echo $query;    
        return $this->conection->runquery($db, $query);
    }
}


?>