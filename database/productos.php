<?php 
require_once ("conection.php");
class Productos{
	private $conection;
    private $nameTable;

    public function __construct(){
        $this->conection = new Conection;
        $this->nameTable = "producto";
    }

    public function getAll(){
        $db = $this->conection->initConection();
        $query = "SELECT id, nombre, descripcion, estado FROM {$this->nameTable}";
        return $this->conection->runquery($db, $query);
    }

    public function getID($id){
        $db = $this->conection->initConection();
        $query = "SELECT id, nombre, descripcion, estado FROM ".$this->nameTable." WHERE id = ".$id.";";
        return $this->conection->runquery($db, $query);
    }

    public function create($data){
        $db = $this->conection->initConection();
        $query = "INSERT INTO ".$this->nameTable." (nombre, descripcion, estado) 
                VALUES ('".$data['nombre']."', '".$data['descripcion']."', ".$data['estado'].");";
        return $this->conection->runquery($db, $query);
    }

    public function edit($id, $data){
        $db = $this->conection->initConection();
        $query = "UPDATE ".$this->nameTable." SET nombre = '".$data['nombre']."', descripcion = '".$data['descripcion']."', 
                estado = ".$data['estado']." WHERE id = ".$id.";";
        return $this->conection->runquery($db, $query);
    }

    public function delete(){

    }
}


?>