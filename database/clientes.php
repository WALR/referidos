<?php 
require_once ("conection.php");
date_default_timezone_set('America/Guatemala');

class Clientes{
	private $conection;
    private $nameTable;

    public function __construct(){
        $this->conection = new Conection;
        $this->nameTable = "cliente";
    }


    public function getAll(){
        $db = $this->conection->initConection();
        $query = "SELECT * FROM {$this->nameTable}";
        return $this->conection->runquery($db, $query);
    }

    public function getID($id){
        $db = $this->conection->initConection();
        $query = "SELECT * FROM ".$this->nameTable." WHERE id = ".$id.";";
        return $this->conection->runquery($db, $query);
    }

    public function create($data){
        $result = $this->checkClienteExiste($data);
        if($result->num_rows > 0){
            $cliente = $result->fetch_assoc();
            return array('code'=> 2, 'data'=> $cliente);
        }else{
            $db = $this->conection->initConection();
            $Fnacimiento = DateTime::createFromFormat('d/m/Y', $data['fecha_nacimiento'])->format('Y-m-d');
            $query = "INSERT INTO ".$this->nameTable." 
                    (primer_nombre, segundo_nombre, primer_apellido, segundo_apellido, 
                    fecha_nacimiento, dpi, telefono, celular, 
                    direccion, estado) 
                    VALUES 
                    ('".$data['primer_nombre']."', '".$data['segundo_nombre']."', '".$data['primer_apellido']."', '".$data['segundo_apellido']."', 
                    '".$Fnacimiento."', ".((empty($data['dpi']))?'null':$data['dpi']).", ".((empty($data['telefono']))?'null':$data['telefono']).", ".((empty($data['celular']))?'null':$data['celular']).",
                    '".$data['direccion']."', 1);";
            // echo $query;SELECT LAST_INSERT_ID();
            if($this->conection->runquery($db, $query)){
                $result = $this->conection->runquery($db, "SELECT LAST_INSERT_ID()");
                $idCliente = $result->fetch_assoc();
                $clienteResponse = array('id'=>$idCliente['LAST_INSERT_ID()'], 'primer_nombre'=>$data['primer_nombre'], 'primer_apellido'=>$data['primer_apellido']);
                // var_dump($idCliente);
                if($data['referencia']>0){
                    if($this->agregarReferencia($idCliente['LAST_INSERT_ID()'], $data['referencia'], $data['notas'], $_SESSION['id'])){
                        return array('code'=> 1, 'data'=> $clienteResponse);
                    }else {
                        return array('code'=> 3, 'data'=> $clienteResponse);
                    };
                }else {
                    return array('code'=> 1, 'data'=> $clienteResponse);
                }
            }else {
                return array('code'=> 4, 'data'=> '');
            }
        }
    }

    public function edit($id, $data){
        $db = $this->conection->initConection();
        $Fnacimiento = DateTime::createFromFormat('d/m/Y', $data['fecha_nacimiento'])->format('Y-m-d');
        $estado = ((isset($data['estado']))? ",estado = ".$data['estado']: "");
        $query = "UPDATE ".$this->nameTable." SET
                primer_nombre = '".$data['primer_nombre']."', segundo_nombre = '".$data['segundo_nombre']."', primer_apellido = '".$data['primer_apellido']."',
                segundo_apellido = '".$data['segundo_apellido']."', fecha_nacimiento = '".$Fnacimiento."', dpi = ".((empty($data['dpi']))?'null':$data['dpi']).",
                telefono = ".((empty($data['telefono']))?'null':$data['telefono']).", celular = ".((empty($data['celular']))?'null':$data['celular']).", direccion = '".$data['direccion']."' 
                ".$estado." WHERE id = ".$id.";";
        return $this->conection->runquery($db, $query);
    }

    public function buscarCliente($data){
        $db = $this->conection->initConection();
        $Snombre = ((empty($data['segundo_nombre']))? "": "OR segundo_nombre LIKE '".$data['segundo_nombre']."' ");
        $Sapellido = ((empty($data['segundo_apellido']))? "": "OR segundo_apellido LIKE '".$data['segundo_apellido']."' ");
        $dpi = ((empty($data['dpi']))? "": "AND dpi = ".$data['dpi']);
        if(!empty($data['fecha_nacimiento'])){
            $FechaFormat = DateTime::createFromFormat('d/m/Y', $data['fecha_nacimiento'])->format('Y-m-d');
            $Fnacimiento = "AND fecha_nacimiento ='".$FechaFormat."'";
        }else {
            $Fnacimiento = "";
        }
        
        $query = "SELECT * FROM ".$this->nameTable." WHERE primer_nombre LIKE '%".$data['primer_nombre']."%' OR primer_apellido LIKE '%".$data['primer_apellido']."%' 
                ".$Snombre." ".$Sapellido." ".$dpi." ".$Fnacimiento.";";
        
        // echo $query;
        return $this->conection->runquery($db, $query);
    }

    public function delete(){

    }

    public function agregarReferencia($idCliente, $idProducto, $notas, $idUsuario){
        $hoy = date("Y-m-d H:i:s");
        $db = $this->conection->initConection();
        $query = "INSERT INTO referencia (fecha, id_cliente, id_producto, id_usuario, notas, estado) 
                VALUES ('".$hoy."', ".$idCliente.", ".$idProducto.", ".$idUsuario.", '".$notas."', 1);";
        return $this->conection->runquery($db, $query);
    }


    public function checkClienteExiste($data){
        $Fnacimiento = DateTime::createFromFormat('d/m/Y', $data['fecha_nacimiento'])->format('Y-m-d');
        $db = $this->conection->initConection();
        $query = "SELECT id, primer_nombre, primer_apellido, fecha_nacimiento FROM ".$this->nameTable." 
                WHERE primer_nombre = '".$data['primer_nombre']."' AND primer_apellido = '".$data['primer_apellido']."' 
                AND fecha_nacimiento = '".$Fnacimiento."' ;";
        // return $this->conection->runquery($db, $query);
        // $result =  
        return $this->conection->runquery($db, $query);
        
    }

    public function getProductosNoReferidos($idCliente){
        $db = $this->conection->initConection();
        $query = "SELECT id, nombre FROM producto WHERE estado = 1 
                AND id NOT IN 
                (SELECT id_producto FROM referencia WHERE estado = 1 AND id_cliente = ".$idCliente.")";
        return $this->conection->runquery($db, $query);
    }

    public function getClienteReferencias($idCliente){
        $db = $this->conection->initConection();
        $query ="SELECT r.id, r.fecha, r.notas, r.estado, r.comentario_estado, p.nombre as nombre_producto, 
                u.nombre as nombre_usuario, u.id as id_usuario, id_usuario_estado, u2.nombre as nombre_estado
                FROM referencia r INNER JOIN producto p ON p.id = r.id_producto 
                INNER JOIN usuario u ON u.id = r.id_usuario
                LEFT JOIN usuario u2 ON u2.id = r.id_usuario_estado
                WHERE id_cliente =".$idCliente." ORDER BY r.fecha DESC;";
        return $this->conection->runquery($db, $query);
    }

    public function getClienteBitacoras($idCliente){
        $db = $this->conection->initConection();
        $query ="SELECT b.id, b.fecha, b.comentario, b.id_usuario, u.nombre as nombre_usuario 
                FROM bitacora b INNER JOIN usuario u ON u.id = b.id_usuario 
                WHERE id_cliente =".$idCliente." ORDER BY b.fecha DESC;";
        return $this->conection->runquery($db, $query);
    }

    public function deleteReferencia($idReferencia){
        $db = $this->conection->initConection();
        $query ="DELETE FROM referencia WHERE id =".$idReferencia.";";
        return $this->conection->runquery($db, $query);
    }

    public function cambiarEstadoReferencia($idReferencia, $estado, $comentarioEstado, $idUsuario){
        $db = $this->conection->initConection();
        $query ="UPDATE referencia SET estado =".$estado.", comentario_estado = '".$comentarioEstado."',
                id_usuario_estado = ".$idUsuario." WHERE id =".$idReferencia.";";
        return $this->conection->runquery($db, $query);
    }

    public function agregarBitacora($idCliente, $comentario, $idUsuario){
        $hoy = date("Y-m-d H:i:s");
        $db = $this->conection->initConection();
        $query = "INSERT INTO bitacora (fecha, id_cliente, id_usuario, comentario) 
                VALUES ('".$hoy."', ".$idCliente.", ".$idUsuario.", '".$comentario."');";
        return $this->conection->runquery($db, $query);
    }

    public function deleteBitacora($idBitacora){
        $db = $this->conection->initConection();
        $query ="DELETE FROM bitacora WHERE id =".$idBitacora.";";
        return $this->conection->runquery($db, $query);
    }
    
}


?>